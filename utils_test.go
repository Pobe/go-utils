package utils

import "testing"

func TestRound(t *testing.T) {
	var got float64
	var exp float64

	got = Round(10.0, 2)
	exp = 10.0
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}

	got = Round(10.5, 0)
	exp = 11.0
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}

	got = Round(10.1, 0)
	exp = 10.0
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}

	got = Round(10.5, 9)
	exp = 10.5
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}

	got = Round(10.1, 1)
	exp = 10.1
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}

	got = Round(10.15, 1)
	exp = 10.2
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}

	got = Round(10.15, 2)
	exp = 10.15
	if got != exp {
		t.Errorf("Was supposed to get %f, had : %f", exp, got)
	}
}

func TestByteSizeToVal(t *testing.T) {
	var got string
	var exp string

	got = ByteSizeToVal(0)
	exp = "0B"
	if got != exp {
		t.Errorf("Was supposed to get %s, had : %s", exp, got)
	}

	got = ByteSizeToVal(1)
	exp = "1.00B"
	if got != exp {
		t.Errorf("Was supposed to get %s, had : %s", exp, got)
	}

	got = ByteSizeToVal(156628)
	exp = "152.96KB"
	if got != exp {
		t.Errorf("Was supposed to get %s, had : %s", exp, got)
	}

	got = ByteSizeToVal(1566667)
	exp = "1.49MB"
	if got != exp {
		t.Errorf("Was supposed to get %s, had : %s", exp, got)
	}

	got = ByteSizeToVal(1500000000000)
	exp = "1.36TB"
	if got != exp {
		t.Errorf("Was supposed to get %s, had : %s", exp, got)
	}

	got = ByteSizeToVal(1500000000000000)
	exp = "1.33PB"
	if got != exp {
		t.Errorf("Was supposed to get %s, had : %s", exp, got)
	}
}

func TestValSizeToBytes(t *testing.T) {
	var got int64
	var exp int64

	got = ValSizeToBytes("12KB")
	exp = 12288
	if got != exp {
		t.Errorf("Was supposed to get %d, had : %d", exp, got)
	}

	got = ValSizeToBytes("122B")
	exp = 122
	if got != exp {
		t.Errorf("Was supposed to get %d, had : %d", exp, got)
	}

	got = ValSizeToBytes("12 KB")
	exp = 12288
	if got != exp {
		t.Errorf("Was supposed to get %d, had : %d", exp, got)
	}

	got = ValSizeToBytes("12.12 KB")
	exp = 12410
	if got != exp {
		t.Errorf("Was supposed to get %d, had : %d", exp, got)
	}

	got = ValSizeToBytes("12.12 kb")
	exp = 12410
	if got != exp {
		t.Errorf("Was supposed to get %d, had : %d", exp, got)
	}

	got = ValSizeToBytes("1.1PB")
	exp = 1238489897526886
	if got != exp {
		t.Errorf("Was supposed to get %d, had : %d", exp, got)
	}
}
