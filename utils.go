package utils

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
)

// StringInSlice look for string "a" in string list.
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}

	return false
}

// StringIndexInSlice takes a string "a" and look in "list" to find it.
// return the position if found -1 if not
func StringIndexInSlice(a string, list []string) int {
	for i, b := range list {
		if b == a {
			return i
		}
	}

	return -1
}

var suffixes = [6]string{"B", "KB", "MB", "GB", "TB", "PB"}

// ValSizeToBytes takes a string value composed of a float
// and size value (B, KB, MB, etc.)
// Return a int64 bytes value or -1 if the matches doesn't fit
// Valid input: 12KB, 12.12 KB, 12 kb
// Invalid input: 0.23KB
func ValSizeToBytes(valSize string) int64 {
	valSize = strings.TrimSpace(valSize)
	r, _ := regexp.Compile("([1-9]+\\.?[0-9]*) ?([A-Za-z]{1,2})")
	matches := r.FindStringSubmatch(valSize) //[string, size, suffix]

	if len(matches) != 3 {
		return -1
	}

	size, _ := strconv.ParseFloat(matches[1], 64)
	pow := StringIndexInSlice(strings.ToUpper(matches[2]), suffixes[:])

	return int64(size * float64(math.Pow(1024, float64(pow))))
}

// ByteSizeToVal takes a size in byte and give a string value of style 12.23MB.
// Up to Petabytes value.
func ByteSizeToVal(size float64) string {
	if size == 0 {
		return "0B"
	}

	suffixIndex := math.Floor(math.Log(float64(size)) / math.Log(1024))
	power := math.Pow(1024, suffixIndex)
	convertedSize := Round(size/power, 2)
	return fmt.Sprintf("%.2f%s", convertedSize, suffixes[int(math.Floor(suffixIndex))])
}

// Round takes a value and round on based on places.
// Round(10.1, 0) => 10.0, Round(10.1, 1) => 10.1, Round(10.5, 1) => 10.5
func Round(val float64, places int) float64 {
	var round float64
	roundOn := 0.5
	pow := math.Pow(10, float64(places))
	digit := pow * val
	_, div := math.Modf(digit) //The fraction
	if div >= roundOn {
		round = math.Ceil(digit)
	} else {
		round = math.Floor(digit)
	}
	return round / pow
}
